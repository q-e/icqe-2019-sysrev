# Quantitative Ethnography Conceptualization And Reporting Systematic Review

## Introduction

This file is part of the repository hosted at https://gitlab.com/q-e/icqe-2019-sysrev, and its rendered version is hosted by GitLab Pages at https://q-e.gitlab.io/icqe-2019-sysrev. The Google Sheet holding the extraction script template is available from https://docs.google.com/spreadsheets/d/1n6bZaWyHAHTw6pzoS9Z5IaZ_i0xiW3XHfrir2NfPSO8.

## Authors

Savannah Donegan (University of Wisconsin-Madison), Brendan Eagan (University of Wisconsin-Madison), Anna Geröly (ELTE, Hungary), Anna Jeney (ELTE, Hungary), Susan Jiao (University of Wisconsin-Madison), Gjalt-Jorn Peters (Open University of the Netherlands), Clare Porter (University of Wisconsin-Madison), Szilvia Zörgő (Semmelweis University, Hungary)
